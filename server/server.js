const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

app.get('/', (req, res) => res.send('Hello, from the Server!'));

//Fire up the server
const port = process.env.PORT || 4000;

app.listen(port, () => {
	console.log(`App listening at http://localhost:${port}`);
});
